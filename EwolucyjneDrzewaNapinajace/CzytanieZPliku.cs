﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;
using System.IO;

namespace EwolucyjneDrzewaNapinajace
{
    class CzytanieZPliku
    {
        public int[][] WczytajPlik(int[][] F)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".txt";
            dialog.Filter = "TXT Files (*.txt)|*.txt";
            Nullable<bool> wynik = dialog.ShowDialog();
            if (wynik == true)
            {
                StreamReader reader = new StreamReader(dialog.FileName);
                string przeczytane = reader.ReadToEnd();
                CzytanieZPliku tworzenieTablicy = new CzytanieZPliku();
                F = tworzenieTablicy.StworzTablice(przeczytane);
            }
            return F;
        }

        public int[][] StworzTablice(string przeczytane)
        {
            //szukaj enterów - ile linii tyle wierzchołków
            string[] Linie = przeczytane.Split('\n');
            int iloscWierzcholkow = Linie.Length;

            //szukaj spacji i enterów - stwórz tablice wszystkich wag oddzielonych przecinkami dla jednej krawędzi
            string[] wszystkieWagiKrawedzi = przeczytane.Split(new string[] { ",", "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

            int[][] F = new int[iloscWierzcholkow][];
            for (int i = 0; i < iloscWierzcholkow; i++)
            {
                F[i] = new int[iloscWierzcholkow];
                for (int j = 0; j < iloscWierzcholkow; j++)
                {                    
                    F[i][j] = Convert.ToInt32(wszystkieWagiKrawedzi[iloscWierzcholkow * i + j]);
                }
            }
            return F;            
        }
    }
}
