﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace EwolucyjneDrzewaNapinajace
{
    public class ZapisDoPliku
    {
        public void ZapiszDane(Zycie zycie, Waga[][] wagi)
        {
            Pareto pareto = new Pareto();
            string sciezka = Environment.CurrentDirectory + "\\pareto.txt";

            List<List<Wpis>> kody = new List<List<Wpis>>(zycie.pokolenie);
            int maxDlugoscKodu = -1;

            for (int i = 1; i <= zycie.pokolenie; i++)
            {
                kody.Add(new List<Wpis>(zycie.populacja.Count));
                List<Drzewo> frontPareto = pareto.WyznaczFrontPareto(zycie.ZwrocPopulacje(i), wagi);
                for (int j = 0; j < frontPareto.Count; j++)
                {
                    string kod = frontPareto[j].KodDrzewa();
                    if (maxDlugoscKodu < kod.Length)
                    {
                        maxDlugoscKodu = kod.Length;
                    }
                    kody[i-1].Add(new Wpis() { kod = kod, F1 = frontPareto[j].F1(wagi), F2 = frontPareto[j].F2(wagi)});
                }
            }

            StreamWriter sw = new StreamWriter(sciezka);
            sw.WriteLine("# PARETO FRONT RESULTS");
            string drugaLinia = "# Tree code";
            int dlugosc = drugaLinia.Length;
            for (int i = 0; i < maxDlugoscKodu - dlugosc; i++)
            {
                drugaLinia += " ";
            }
            drugaLinia += "   F1   F2";
            sw.WriteLine(drugaLinia);
            for (int i = 0; i < kody.Count; i++)
            {
                int populacja = i + 1;
                sw.WriteLine();
                sw.WriteLine("# Population " + populacja.ToString());
                foreach (var item in kody[i])
                {
                    string linia = item.kod;
                    dlugosc = linia.Length;
                    for (int j = 0; j < maxDlugoscKodu - dlugosc; j++)
                    {
                        linia += ' ';
                    }
                    if (item.F1<1000)
                    {
                        linia += " ";
                    }
                    linia += " " + item.F1.ToString();
                    if (item.F2 < 1000)
                    {
                        linia += " ";
                    }
                    linia += " " + item.F2.ToString();
                    sw.WriteLine(linia);
                }
            }
            sw.Close();
            Process.Start("notepad.exe", sciezka);
        }

        private class Wpis
        {
            public string kod;
            public int F1;
            public int F2;
        }
    }
}
