﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.ComponentModel;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>    
    public partial class MainWindow : Window
    {
        public Zycie zycie;
        public int[][] F1;
        public int[][] F2;
        public int wyswietlanePokolenie = 0;
        public BackgroundWorker liczenieGrafow;
        private ManualResetEvent liczenieGrafowSemafor;

        public MainWindow()
        {
            InitializeComponent();
            liczenieGrafow = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };
            liczenieGrafowSemafor = new ManualResetEvent(true);
        }

        private void JednoPokolenie(bool ewoluuj)
        {
            if (ewoluuj)
            {
                zycie.JednoPokolenie();
            }
            RysujWszystko();
        }

        private void OdswiezLabeleGeneracji()
        {
            labelNumerGeneracji.Dispatcher.Invoke((ThreadStart)delegate { labelNumerGeneracji.Content = "Generation number: " + wyswietlanePokolenie; });
            labelNumerGeneracji2.Dispatcher.Invoke((ThreadStart)delegate { labelNumerGeneracji2.Content = "Generation number: " + wyswietlanePokolenie; });
        }

        /// <summary>
        /// Funkcja generująca określoną ilość pokoleń co określony czas.
        /// </summary>
        /// <param name="liczbaPokolen">Liczba pokoleń do wygenerowania.</param>
        /// <param name="interwal">Przerwa między pokoleniami w ms.</param>
        private void WielePokolen()
        {
            int iloscGeneracji = 0;
            int czasMiedzyGeneracjami = 0;
            textBoxGenerationsAmount.Dispatcher.Invoke((ThreadStart)delegate { iloscGeneracji = Convert.ToInt32(textBoxGenerationsAmount.Text); });
            textBoxGenerationInterval.Dispatcher.Invoke((ThreadStart)delegate { czasMiedzyGeneracjami = Convert.ToInt32(textBoxGenerationInterval.Text); });
            for (int i = 0; i < iloscGeneracji-1; i++)
            {
                if (liczenieGrafow.CancellationPending)
                {
                    return;
                }
                liczenieGrafowSemafor.WaitOne();
                JednoPokolenie(true);
                wyswietlanePokolenie++;
                OdswiezLabeleGeneracji();
                Thread.Sleep(czasMiedzyGeneracjami);
            }
        }

        private void RysujWszystko()
        {
            RysujDrzewa();
            RysujPopulacje();
        }

        private void RysujDrzewa()
        {
            CanvasNajlepszeDrzewo.Dispatcher.Invoke((ThreadStart)delegate { RysujDrzewo(zycie.ZwrocNajlepszeDrzewo(wyswietlanePokolenie), CanvasNajlepszeDrzewo); });
            labelOcenaNajlepszego.Dispatcher.Invoke((ThreadStart)delegate { WypiszNajlepszaFunkcjeOceny(zycie.ZwrocNajlepszeDrzewo(wyswietlanePokolenie).Ocena(zycie.wagi)); });
            CanvasSrednieDrzewo.Dispatcher.Invoke((ThreadStart)delegate { RysujDrzewo(zycie.ZwrocSrednieDrzewo(wyswietlanePokolenie), CanvasSrednieDrzewo); });
            labelOcenaSredniego.Dispatcher.Invoke((ThreadStart)delegate { WypiszSredniaFunkcjeOceny(zycie.ZwrocSrednieDrzewo(wyswietlanePokolenie).Ocena(zycie.wagi)); });
            CanvasNajgorszeDrzewo.Dispatcher.Invoke((ThreadStart)delegate { RysujDrzewo(zycie.ZwrocNajgorszeDrzewo(wyswietlanePokolenie), CanvasNajgorszeDrzewo); });
            labelOcenaNajgorszego.Dispatcher.Invoke((ThreadStart)delegate { WypiszNajgorszaFunkcjeOceny(zycie.ZwrocNajgorszeDrzewo(wyswietlanePokolenie).Ocena(zycie.wagi)); });
        }

        private void RysujPopulacje()
        {
            Pareto pareto = new Pareto();
            int iloscGeneracji = 0;
            textBoxGenerationsAmount.Dispatcher.Invoke((ThreadStart)delegate { iloscGeneracji = Convert.ToInt32(textBoxGenerationsAmount.Text); });
            CanvasWykresGeneracji.Dispatcher.Invoke((ThreadStart)delegate { RysujPopulacje(zycie.ZwrocPopulacje(wyswietlanePokolenie), CanvasWykresGeneracji, wyswietlanePokolenie, iloscGeneracji); });
            CanvasWykresPareto.Dispatcher.Invoke((ThreadStart)delegate { RysujPopulacje(pareto.WyznaczFrontPareto(zycie.ZwrocPopulacje(wyswietlanePokolenie), zycie.wagi), CanvasWykresPareto, wyswietlanePokolenie, iloscGeneracji); });
        }

        public void ZmienWidocznoscKontrolek(bool parametr)
        {
            labelMaxVertexDegree.IsEnabled = parametr;
            textBoxVertexDegree.IsEnabled = parametr;
            labelExecutionType.IsEnabled = parametr;
            labelGenerationInterval.IsEnabled = parametr;
            labelGenerationsAmount.IsEnabled = parametr;
            labelMs.IsEnabled = parametr;
            labelMutationProbability.IsEnabled = parametr;
            labelPopuationSize.IsEnabled = parametr;
            labelProcent.IsEnabled = parametr;
            labelSuccessionType.IsEnabled = parametr;
            textBoxGenerationInterval.IsEnabled = parametr;
            textBoxGenerationsAmount.IsEnabled = parametr;
            textBoxMutationProbability.IsEnabled = parametr;
            textBoxPopulationSize.IsEnabled = parametr;
            radioButtonAllAtOnce.IsEnabled = parametr;
            radioButtonCustomSuccession.IsEnabled = parametr;
            radioButtonTournamentSuccession.IsEnabled = parametr;
            radioButtonStepByStep.IsEnabled = parametr;
            if (parametr==true)
            {
                przyciskStart.Visibility = Visibility.Visible;
            }
            else
            {
                przyciskStart.Visibility = Visibility.Hidden;
            }
        }      

        /// <summary>
        /// Wypisuje pod wykresem wartość funkcji oceny dla najlepszego drzewa
        /// </summary>
        /// <param name="ocena">Ocena najlepszego drzewa</param>
        public void WypiszNajlepszaFunkcjeOceny(int ocena)
        {
            labelOcenaNajlepszego.Content = "THE BEST TREE (Fitness: " + ocena +")";
        }

        /// <summary>
        /// Wypisuje pod wykresem wartość funkcji oceny dla średniego drzewa
        /// </summary>
        /// <param name="ocena">Ocena średniego drzewa</param>
        public void WypiszSredniaFunkcjeOceny(int ocena)
        {
            labelOcenaSredniego.Content = "MEDIUM TREE (Fitness: " + ocena +")";
        }

        /// <summary>
        /// Wypisuje pod wykresem wartość funkcji oceny dla najgorszego drzewa
        /// </summary>
        /// <param name="ocena">Ocena najgorszego drzewa</param>
        public void WypiszNajgorszaFunkcjeOceny(int ocena)
        {
            labelOcenaNajgorszego.Content = "THE WORST TREE (Fitness: " + ocena + ")";
        }

        public void RysujDrzewo(Drzewo drzewo, Canvas okno)
        {
            List<Krawedz> krawedzie = new List<Krawedz>(drzewo.krawedzie);
            List<Wierzcholek> wierzcholki = new List<Wierzcholek>();
            Dictionary<int, Point> wspolrzedne = new Dictionary<int, Point>();

            foreach (Krawedz krawedz in krawedzie)
            {
                if (!wierzcholki.Any(x=>x.numer == krawedz.wierzcholek1.numer))
                {
                    wierzcholki.Add(krawedz.wierzcholek1);
                }
                if (!wierzcholki.Any(x => x.numer == krawedz.wierzcholek2.numer))
                {
                    wierzcholki.Add(krawedz.wierzcholek2);
                }
            }
            wierzcholki = wierzcholki.OrderBy(x => x.numer).ToList();

            okno.Children.Clear();
            Ellipse okrag = new Ellipse();
            double promien = (okno.ActualHeight >= okno.ActualWidth ? okno.ActualWidth : okno.ActualHeight) / 2 - 10;
            okrag.Height = 2 * promien;
            okrag.Width = 2 * promien;
            okrag.StrokeThickness = 2;
            okrag.Stroke = Brushes.Gray;
            double odstepPion = (okno.ActualHeight - okrag.Height) / 2;
            double odstepPoziom = (okno.ActualWidth - okrag.Width) / 2;
            Panel.SetZIndex(okrag, 0);
            Canvas.SetTop(okrag, odstepPion);
            Canvas.SetLeft(okrag, odstepPoziom);
            for (int i = 0; i < wierzcholki.Count; i++)
            {
                Ellipse punkt = new Ellipse
                {
                    Height = 10,
                    Width = 10,
                    StrokeThickness = 10,
                    Stroke = Brushes.Black
                };
                okno.Children.Add(punkt);
                Panel.SetZIndex(punkt, 2);
                Point pozycja = new Point
                {
                    X = promien + promien * Math.Cos(i * (Math.PI / 180) * (360 / (double)wierzcholki.Count)) - punkt.Height / 2,
                    Y = promien + promien * Math.Sin(i * (Math.PI / 180) * (360 / (double)wierzcholki.Count)) - punkt.Width / 2
                };
                wspolrzedne.Add(wierzcholki[i].numer, pozycja);
                Canvas.SetLeft(punkt, pozycja.X + odstepPoziom);
                Canvas.SetTop(punkt, pozycja.Y + odstepPion);
                Label numerWierzcholka = new Label
                {
                    Content = wierzcholki[i].nazwa,
                    FontSize = 20,
                    Width = 32,
                    Height = 32,
                    Background = new SolidColorBrush(Color.FromArgb(50, 255, 255, 255))
                };
                Panel.SetZIndex(numerWierzcholka, 4);
                Canvas.SetLeft(numerWierzcholka, (promien + promien * Math.Cos(i * (Math.PI / 180) * (360 / (double)wierzcholki.Count)) + numerWierzcholka.ActualWidth / 2) + odstepPoziom);
                Canvas.SetTop(numerWierzcholka, (promien + promien * Math.Sin(i * (Math.PI / 180) * (360 / (double)wierzcholki.Count)) + numerWierzcholka.ActualHeight / 2) + odstepPion);
                okno.Children.Add(numerWierzcholka);
            }

            foreach (Krawedz krawedz in krawedzie)
            {
                Line linia = new Line
                {
                    StrokeThickness = 3,
                    Stroke = new SolidColorBrush(ZwrocKolor(krawedz.F1(zycie.wagi) + krawedz.F2(zycie.wagi), 0, 200))
                };
                linia.X1 = wspolrzedne[krawedz.wierzcholek1.numer].X + 5;
                linia.Y1 = wspolrzedne[krawedz.wierzcholek1.numer].Y + 5;
                linia.X2 = wspolrzedne[krawedz.wierzcholek2.numer].X + 5;
                linia.Y2 = wspolrzedne[krawedz.wierzcholek2.numer].Y + 5;
                okno.Children.Add(linia);
                Panel.SetZIndex(linia, 1);
                Canvas.SetTop(linia, odstepPion);
                Canvas.SetLeft(linia, odstepPoziom);
            }

            okno.Tag = drzewo;
        }

        private byte Interpolacja(byte a, byte b, double p)
        {
            return (byte)(a * (1 - p) + b * p);
        }

        private Color ZwrocKolor(int wartosc, int minimum, int maksimum)
        {
            SortedDictionary<int, Color> d = new SortedDictionary<int, Color>
            {
                { minimum, Color.FromRgb(0, 255, 0) },
                { Math.Abs(maksimum - minimum) / 2, Color.FromRgb(255, 255, 0) },
                { maksimum, Color.FromRgb(255, 0, 0) }
            };
            KeyValuePair<int, Color> kvp_previous = new KeyValuePair<int, Color>(-1, Color.FromRgb(0, 0, 0));
            foreach (KeyValuePair<int, Color> kvp in d)
            {
                if (kvp.Key > wartosc)
                {
                    double p = (wartosc - kvp_previous.Key) / (double)(kvp.Key - kvp_previous.Key);
                    Color a = kvp_previous.Value;
                    Color b = kvp.Value;
                    Color c = Color.FromRgb(
                        Interpolacja(a.R, b.R, p),
                        Interpolacja(a.G, b.G, p),
                        Interpolacja(a.B, b.B, p));
                    return c;
                }
                kvp_previous = kvp;
            }

            return Color.FromRgb(0, 0, 0);
        }

        public void RysujPopulacje(List<Drzewo> populacja, Canvas okno, int numerPopulacji, int maxPopulacji)
        {
            foreach (Drzewo osobnik in populacja)
            {
                Ellipse punkt = new Ellipse
                {
                    Height = 10,
                    Width = 10,
                    StrokeThickness = 10,
                    Stroke = new SolidColorBrush(ZwrocKolor(numerPopulacji - 1, maxPopulacji, 0)),
                    Tag = new List<object> { numerPopulacji, osobnik }
                };
                punkt.MouseDown += PowiekszPunktPareto_Click;
                okno.Children.Add(punkt);
                Panel.SetZIndex(punkt, 2);
                Point pozycja = new Point
                {
                    X = (osobnik.F1(zycie.wagi) / (osobnik.krawedzie.Count * 100.0)) * okno.ActualWidth,
                    Y = okno.ActualHeight - ((osobnik.F2(zycie.wagi) / (osobnik.krawedzie.Count * 100.0)) * okno.ActualHeight)
                };
                Canvas.SetLeft(punkt, pozycja.X);
                Canvas.SetTop(punkt, pozycja.Y);
            }
        }

        public void RysujWynikiFunkcji(Canvas okno, Drzewo drzewo, Waga[][] wagi)
        {
            TextBlock text = new TextBlock()
            {
                Text = "F1: " + drzewo.F1(wagi) + " " + "F2: " + drzewo.F2(wagi),
                FontSize = 25
            };
            okno.Children.Add(text);
            text.Measure(okno.RenderSize);
            Canvas.SetLeft(text, okno.ActualWidth-text.DesiredSize.Width-7);
            Canvas.SetTop(text, 7);
        }
    }
}
