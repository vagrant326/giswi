﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Interfejs grupujacy klasy obsługujące mutacje drzew.
    /// </summary>
    public interface IMutacja
    {
        /// <summary>
        /// Implementacja ewolucyjnego algorytmu mutacji osobników.
        /// </summary>
        /// <param name="krawedzie">Lista krawędzi drzewa która zostanie zmutowana.</param>
        void Mutuj(List<Krawedz> krawedzie, int maxSasiadow);
    }
}
