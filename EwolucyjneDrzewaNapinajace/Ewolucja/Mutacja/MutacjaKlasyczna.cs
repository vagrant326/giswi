﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasyczna implementacja ewolucyjnej mutacji osobnika.
    /// </summary>
    public class MutacjaKlasyczna : IMutacja
    {
        static int ziarno = 0;
        /// <summary>
        /// Implementacja klasycznego ewolucyjnego algorytmu mutacji osobników.
        /// </summary>
        /// <param name="krawedzie">Lista krawędzi drzewa która zostanie zmutowana.</param>
        public void Mutuj(List<Krawedz> krawedzie, int maxSasiadow)
        {
            Random rand = new Random(ziarno++*1000);
            List<Wierzcholek> wierzcholki = new List<Wierzcholek>();
            foreach (Krawedz krawedz in krawedzie)
            {
                if (!wierzcholki.Any(x=>x.numer == krawedz.wierzcholek1.numer))
                {
                    wierzcholki.Add(krawedz.wierzcholek1);
                }
                if (!wierzcholki.Any(x => x.numer == krawedz.wierzcholek2.numer))
                {
                    wierzcholki.Add(krawedz.wierzcholek2);
                }
            }
            int indeksWylosowanejKrawedzi = rand.Next(0, krawedzie.Count);
            Krawedz wylosowanaKrawedz = krawedzie[indeksWylosowanejKrawedzi];
            Wierzcholek wierzcholekPoczatkowy = wylosowanaKrawedz.wierzcholek1;
            Wierzcholek wierzcholekKoncowy = wylosowanaKrawedz.wierzcholek2;
            wierzcholekPoczatkowy.UsunSasiada(wierzcholekKoncowy);
            wierzcholekKoncowy.UsunSasiada(wierzcholekPoczatkowy);
            krawedzie.RemoveAt(indeksWylosowanejKrawedzi);
            List<Wierzcholek> podgraf1 = new List<Wierzcholek>() { wierzcholekPoczatkowy };
            WyznaczPodgraf(podgraf1, wierzcholki);
            List<Wierzcholek> podgraf2 = new List<Wierzcholek>() { wierzcholekKoncowy };
            WyznaczPodgraf(podgraf2, wierzcholki);
            
            Wierzcholek nowyWierzcholekPoczatkowy;
            Wierzcholek nowyWierzcholekKoncowy;
            do
            {
                int indeksWierzcholkaPodgrafu1 = rand.Next(0, podgraf1.Count);
                int indeksWierzcholkaPodgrafu2 = rand.Next(0, podgraf2.Count);
                nowyWierzcholekPoczatkowy = podgraf1[indeksWierzcholkaPodgrafu1];
                nowyWierzcholekKoncowy = podgraf2[indeksWierzcholkaPodgrafu2];
            } while (nowyWierzcholekPoczatkowy.Sasiedzi.Count >= maxSasiadow || nowyWierzcholekKoncowy.Sasiedzi.Count >= maxSasiadow);
            nowyWierzcholekPoczatkowy.DodajSasiada(nowyWierzcholekKoncowy);
            nowyWierzcholekKoncowy.DodajSasiada(nowyWierzcholekPoczatkowy);
            krawedzie.Add(new Krawedz(nowyWierzcholekPoczatkowy, nowyWierzcholekKoncowy));
        }

        /// <summary>
        /// Funkcja wypełniająca listę wierzchołków ich sąsiadami
        /// </summary>
        /// <param name="podgraf">Lista do wypełnienia. Musi posiadać element początkowy</param>
        private void WyznaczPodgraf(List<Wierzcholek> podgraf, List<Wierzcholek> wszystkieWierzcholki)
        {
            for (int i = 0; i < podgraf.Count; i++)
            {
                foreach (int sasiad in podgraf[i].Sasiedzi)
                {
                    Wierzcholek wierzcholek = wszystkieWierzcholki.Find(x => x.numer == sasiad);
                    if (!podgraf.Any(x=>x.numer == sasiad))
                    {
                        podgraf.Add(wierzcholek);
                    }
                }
            }
        }
    }
}
