﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    public interface ISukcesja
    {
        void Przejdz(List<Drzewo> populacja, Waga[][] wagi);
    }
}
