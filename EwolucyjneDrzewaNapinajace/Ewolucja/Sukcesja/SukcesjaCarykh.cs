﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    class SukcesjaCarykh : ISukcesja
    {
        static int los = 0;
        public void Przejdz(List<Drzewo> populacja, Waga[][] wagi)
        {
            Random rand = new Random(los++);
            populacja = populacja.OrderBy(x => x.Ocena(wagi)).ToList();
            List<int> indeksyDoLosowania = new List<int>(Convert.ToInt32(populacja.Count*5.5));
            List<int> indeksyWylosowaneDoWyrzucenia = new List<int>(populacja.Count/2);
            List<Drzewo> doWyrzucenia = new List<Drzewo>(populacja.Count/2);
            int licznoscPodgrupy = populacja.Count / 10;
            for (int i = 0; i < populacja.Count; i++)
            {
                for (int j = 0; j < i/licznoscPodgrupy+1; j++)
                {
                    indeksyDoLosowania.Add(i);
                }
            }

            for (int i = 0; i < populacja.Count/2;)
            {
                int los = rand.Next(0, indeksyDoLosowania.Count);
                if (!indeksyWylosowaneDoWyrzucenia.Contains(indeksyDoLosowania[los]))
                {
                    indeksyWylosowaneDoWyrzucenia.Add(indeksyDoLosowania[los]);
                    i++;
                }
            }

            foreach (int los in indeksyWylosowaneDoWyrzucenia)
            {
                doWyrzucenia.Add(populacja[los]);
            }

            foreach (Drzewo wyrzucony in doWyrzucenia)
            {
                populacja.Remove(wyrzucony);
            }
            
            //for (int i = 1; i <= 10; i++)
            //{
            //    for (int j = 0; j < licznoscPodgrupy; j++)
            //    {
            //        indeksyDoLosowania.Add(j * i);
            //    }
            //}
            for (int i = 0; i < doWyrzucenia.Count; i++)
            {
                int los = rand.Next(0, populacja.Count / 2);
                populacja.Add((Drzewo)populacja[los].Clone());
            }
        }
    }
}
