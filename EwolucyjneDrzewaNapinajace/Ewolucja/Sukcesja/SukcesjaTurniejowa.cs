﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    class SukcesjaTurniejowa : ISukcesja
    {
        public void Przejdz(List<Drzewo> populacja, Waga[][] wagi)
        {
            Random rand = new Random();
            populacja = populacja.OrderBy(x => rand.Next()).ToList();
            List<Drzewo> nowePokolenie = new List<Drzewo>(populacja.Count);
            List<Drzewo> grupa1 = new List<Drzewo>(populacja.Count / 2);
            List<Drzewo> grupa2 = new List<Drzewo>(populacja.Count / 2);
            for (int i = 0; i < populacja.Count; i++)
            {
                if (i%2==0)
                {
                    grupa1.Add(populacja[i]);
                }
                else
                {
                    grupa2.Add(populacja[i]);
                }
            }
            PrzeprowadzTurniej(grupa1, nowePokolenie, rand, wagi);
            PrzeprowadzTurniej(grupa2, nowePokolenie, rand, wagi);

            populacja = nowePokolenie;
        }

        private void PrzeprowadzTurniej(List<Drzewo> grupa, List<Drzewo> nowePokolenie, Random rand, Waga[][] wagi)
        {
            for (int i = 0; i < grupa.Count; i++)
            {
                int numerPrzeciwnika = i;
                do
                {
                    numerPrzeciwnika = rand.Next(0, grupa.Count);
                }
                while (numerPrzeciwnika == i);
                if (grupa[i].F1(wagi) >= grupa[numerPrzeciwnika].F1(wagi))
                {
                    nowePokolenie.Add((Drzewo)grupa[i].Clone());
                }
                else
                {
                    nowePokolenie.Add((Drzewo)grupa[numerPrzeciwnika].Clone());
                }
            }
        }
    }
}
