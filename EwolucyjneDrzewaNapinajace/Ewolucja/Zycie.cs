﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasa grupująca wszystkie dane i metody związane z algorytmami ewolucyjnymi.
    /// </summary>
    public class Zycie
    {
        /// <summary>
        /// Prywatne pole przechowujące tablicę wag krawędzi.
        /// </summary>
        private Waga[][] _wagi;

        /// <summary>
        /// Tablica wag krawędzi.
        /// </summary>
        public Waga[][] wagi
        {
            get {
                return _wagi;
            }
            private set { }
        }

        private List<List<Drzewo>> historiaCywilizacji;

        /// <summary>
        /// Populacja drzew algorytmu ewolucyjnego.
        /// </summary>
        private List<Drzewo> _populacja;

        public List<Drzewo> populacja
        {
            get
            {
                return _populacja;
            }
            private set { }
        }

        /// <summary>
        /// Algorytm sukcesji populacji.
        /// </summary>
        private ISukcesja sukcesja;

        private double prawdopodobienstwoMutacji;

        public int pokolenie { get; private set; }

        /// <summary>
        /// Funkcja generująca jedno pokolenie populacji.
        /// </summary>
        public void JednoPokolenie()
        {
            Random rand = new Random(pokolenie * 1000);
            sukcesja.Przejdz(_populacja, _wagi);
            _populacja = _populacja.OrderBy(x => x.Ocena(wagi)).ToList();
            for (int i = 1; i < _populacja.Count; i++)//elyta
            {
                if (rand.NextDouble()<prawdopodobienstwoMutacji)
                {
                    _populacja[i].Mutuj();
                }
            }
            SpiszPopulacje();
            pokolenie++;
        }
        
        /// <summary>
        /// Funkcja szukająca najlepszego drzewa w populacji na podstawie jego funkcji oceny
        /// </summary>
        /// <returns>Zwraca najlepsze drzewo</returns>
        public Drzewo ZwrocNajlepszeDrzewo(int generacja)
        {
            Drzewo najlepsze = historiaCywilizacji[generacja-1][0];
            foreach (var item in historiaCywilizacji[generacja-1])
            {
                if (item.Ocena(wagi) < najlepsze.Ocena(wagi))
                {
                    najlepsze = item;
                }
            }
            return najlepsze;
        }

        /// <summary>
        /// Funkcja szukająca najgorszego drzewa w populacji na podstawie jego funkcji oceny
        /// </summary>
        /// <returns>Zwraca najgorsze drzewo</returns>
        public Drzewo ZwrocNajgorszeDrzewo(int generacja)
        {
            Drzewo najgorsze = historiaCywilizacji[generacja - 1][0];
            foreach (var item in historiaCywilizacji[generacja - 1])
            {
                if (item.Ocena(wagi) > najgorsze.Ocena(wagi))
                {
                    najgorsze = item;
                }
            }
            return najgorsze;
        }

        public Drzewo ZwrocSrednieDrzewo(int generacja)
        {
            Drzewo srednie = null;
            List<Drzewo> drzewa = new List<Drzewo>(historiaCywilizacji[generacja-1]);
            drzewa.Sort(new KomparatorDrzew(wagi));
            int iloscDrzew = drzewa.Count;
            int ktoreDrzewo = iloscDrzew / 2;
            srednie = drzewa[ktoreDrzewo];
            return srednie;
        }

        private void SpiszPopulacje()
        {
            List<Drzewo> kopiaListy = new List<Drzewo>(_populacja.Count);
            foreach (Drzewo osobnik in _populacja)
            {
                kopiaListy.Add((Drzewo)osobnik.Clone());
            }
            historiaCywilizacji.Add(kopiaListy);
        }

        public int LicznikZapisanychPopulacji
        {
            get { return historiaCywilizacji.Count; }
            private set { }
        }

        public List<Drzewo> ZwrocPopulacje(int generacja)
        {
            return historiaCywilizacji[generacja-1];
        }

        /// <summary>
        /// Pełny konstruktor inicjalizujący tablicę wag.
        /// </summary>
        /// <param name="f1">Tablica wag F1.</param>
        /// <param name="f2">Tablica wag F2.</param>
        /// <param name="rozmiarPopulacji">Pożądany rozmiar populacji.</param>
        /// <param name="iloscWierzcholkow">Ilość wierzchołków w grafie.</param>
        /// <param name="maxSasiadow">Ograniczenie maksymalnej ilości sąsiadów wierzchołka.</param>
        /// <param name="numerAlgorytmuMutacji">Wybór algorytmu mutacji. 0 - mutacja klasyczna.</param>
        /// <param name="numerAlgorytmuSukcesji">Wybór algorytmu sukcesji. 0 - sukcesja turniejowa, 1 - sukcesjaa carykh.</param>
        public Zycie(int[][] f1, int[][] f2, int rozmiarPopulacji, int maxSasiadow, int numerAlgorytmuMutacji, int prawdopodobienstwoMutacji, int numerAlgorytmuSukcesji) 
            : this(rozmiarPopulacji, f1.GetLength(0), maxSasiadow, numerAlgorytmuMutacji, prawdopodobienstwoMutacji, numerAlgorytmuSukcesji)
        {
            _wagi = new Waga[f1.GetLength(0)][];
            for (int i = 0; i < f1.GetLength(0); i++)
            {
                _wagi[i] = new Waga[f1[0].GetLength(0)];
                for (int j = 0; j < f1[0].GetLength(0); j++)
                {
                    _wagi[i][j] = new Waga(f1[i][j], f2[i][j]);
                }
            }
        }

        /// <summary>
        /// Konstruktor inicjalizujący i wypełniający listę drzew.
        /// </summary>
        /// <param name="rozmiarPopulacji">Ilość drzew w populacji.</param>
        /// <param name="iloscWierzcholkow">Liczba wierzchołków w drzewie.</param>
        /// <param name="maxSasiadow">Maksymalny stopień wierzchołka.</param>
        /// <param name="numerAlgorytmuMutacji">Wybór algorytmu mutacji. 0 - mutacja klasyczna.</param>
        /// <param name="numerAlgorytmuSukcesji">Wybór algorytmu sukcesji. 0 - sukcesja turniejowa, 1 - sukcesjaa carykh.</param>
        private Zycie(int rozmiarPopulacji, int iloscWierzcholkow, int maxSasiadow, int numerAlgorytmuMutacji, int prawdopodobienstwoMutacji, int numerAlgorytmuSukcesji) :this(rozmiarPopulacji)
        {
            IMutacja algorytmMutacji;
            switch (numerAlgorytmuMutacji)
            {
                case 0:
                    {
                        algorytmMutacji = new MutacjaKlasyczna();
                        break;
                    }
                default:
                    throw new ArgumentException("Niepoprawny numer algorytmu mutacji", "numerAlgorytmuMutacji");
            }

            this.prawdopodobienstwoMutacji = prawdopodobienstwoMutacji / 100.0;

            switch (numerAlgorytmuSukcesji)
            {
                case 0:
                    {
                        sukcesja = new SukcesjaTurniejowa();
                        break;
                    }
                case 1:
                    {
                        sukcesja = new SukcesjaCarykh();
                        break;
                    }
                default:
                    throw new ArgumentException("Niepoprawny numer algorytmu sukcesji", "numerAlgorytmuSukcesji");
            }

            for (int i = 0; i < rozmiarPopulacji; i++)
            {
                _populacja.Add(new Drzewo(iloscWierzcholkow, maxSasiadow, algorytmMutacji));
            }
            SpiszPopulacje();
            pokolenie=1;
        }

        /// <summary>
        /// Domyślny konstruktor. Inicjalizuje listy.
        /// </summary>
        /// <param name="rozmiarPopulacji">Ilość drzew w populacji.</param>
        private Zycie(int rozmiarPopulacji)
        {
            _populacja = new List<Drzewo>(rozmiarPopulacji);
            historiaCywilizacji = new List<List<Drzewo>>();
            pokolenie = 0;
        }
    }
}
