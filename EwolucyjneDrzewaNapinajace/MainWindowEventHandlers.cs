﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

namespace EwolucyjneDrzewaNapinajace
{
    public partial class MainWindow
    {
        /// <summary>
        /// Czyta dane od użytkownika i uruchamia program
        /// </summary>
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            int stopienWierzcholkow = Convert.ToInt32(textBoxVertexDegree.Text);
            int rozmiarPopulacji = Convert.ToInt32(textBoxPopulationSize.Text);
            int iloscGeneracji = Convert.ToInt32(textBoxGenerationsAmount.Text);
            int prawdopodobienstwoMutacji = Convert.ToInt32(textBoxMutationProbability.Text);
            int czasMiedzyGeneracjami = Convert.ToInt32(textBoxGenerationInterval.Text);
            int rodzajSukcesji = -1;
            int typWykonania;

            if (radioButtonTournamentSuccession.IsChecked == true)
            {
                rodzajSukcesji = 0;
            }
            else
            {
                rodzajSukcesji = 1;
            }

            if (radioButtonStepByStep.IsChecked == true)
            {
                typWykonania = 0;
                GridNawigacjaStepByStep.Visibility = Visibility.Visible;
            }
            else
            {
                typWykonania = 1;
                GridNawigacjaAllAtOnce.Visibility = Visibility.Visible;
            }

            GridDrzewa.Visibility = Visibility.Visible;
            GridWykresy.Visibility = Visibility.Visible;
            PrzyciskF1.IsEnabled = false;
            PrzyciskF2.IsEnabled = false;
            przyciskRestart.Visibility = Visibility.Visible;
            przyciskZaspisDoPliku.Visibility = Visibility.Visible;
            ZmienWidocznoscKontrolek(false);

            zycie = new Zycie(F1, F2, rozmiarPopulacji, stopienWierzcholkow, 0, prawdopodobienstwoMutacji, rodzajSukcesji);
            wyswietlanePokolenie=1;
            if (typWykonania == 0)
            {
                JednoPokolenie(false);
                OdswiezLabeleGeneracji();
            }
            else
            {
                liczenieGrafow.DoWork += LiczenieGrafow_DoWork;
                liczenieGrafow.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Wczytuje z pliku wartości wag F1
        /// </summary>
        private void PrzyciskF1_Click(object sender, RoutedEventArgs e)
        {
            CzytanieZPliku tworzenieTablicy = new CzytanieZPliku();
            F1 = tworzenieTablicy.WczytajPlik(F1);
            PrzyciskF1.Content = "F1 LOADED";
            if (F1 != null && F2 != null)
            {
                ZmienWidocznoscKontrolek(true);
            }
        }

        /// <summary>
        /// Wczytuje z pliku wartości wag F2
        /// </summary>
        private void PrzyciskF2_Click(object sender, RoutedEventArgs e)
        {
            CzytanieZPliku tworzenieTablicy = new CzytanieZPliku();
            F2 = tworzenieTablicy.WczytajPlik(F2);
            PrzyciskF2.Content = "F2 LOADED";
            if (F1 != null && F2 != null)
            {
                ZmienWidocznoscKontrolek(true);
            }
        }

        private void SaveResults_Click(object sender, RoutedEventArgs e)
        {
            ZapisDoPliku zapis = new ZapisDoPliku();
            zapis.ZapiszDane(zycie, zycie.wagi);
        }

        /// <summary>
        /// Przechodzi do poprzedniej generacji
        /// </summary>
        private void PrzyciskPoprzedniaGeneracja_Click(object sender, RoutedEventArgs e)
        {
            if (wyswietlanePokolenie==1)
            {
                return;
            }
            List<Ellipse> punktyDoWyrzucenia = CanvasWykresGeneracji.Children.OfType<Ellipse>().Where(x => (int)((List<object>)x.Tag)[0] == wyswietlanePokolenie).ToList();
            List<Ellipse> paretoDoWyrzucenia = CanvasWykresPareto.Children.OfType<Ellipse>().Where(x => (int)((List<object>)x.Tag)[0] == wyswietlanePokolenie).ToList();
            wyswietlanePokolenie--;
            RysujDrzewa();
            foreach (Ellipse punkt in punktyDoWyrzucenia)
            {
                CanvasWykresGeneracji.Children.Remove(punkt);
            }
            foreach (Ellipse punkt in paretoDoWyrzucenia)
            {
                CanvasWykresPareto.Children.Remove(punkt);
            }
            OdswiezLabeleGeneracji();
        }

        /// <summary>
        /// Przechodzi do następnej generacji
        /// </summary>
        private void PrzyciskNastepnaGeneracja_Click(object sender, RoutedEventArgs e)
        {
            if (wyswietlanePokolenie==Convert.ToInt32(textBoxGenerationsAmount.Text))
            {
                return;
            }
            wyswietlanePokolenie++;
            if (zycie.pokolenie > wyswietlanePokolenie-1)
            {
                JednoPokolenie(false);
            }
            else
            {
                JednoPokolenie(true);
            }
            OdswiezLabeleGeneracji();
        }

        private void PrzyciskPause_Click(object sender, RoutedEventArgs e)
        {
            liczenieGrafowSemafor.Reset();
        }

        private void PrzyciskPlay_Click(object sender, RoutedEventArgs e)
        {
            liczenieGrafowSemafor.Set();
        }

        private void Restart_Click(object sender, RoutedEventArgs e)
        {
            ZmienWidocznoscKontrolek(true);
            PrzyciskF1.IsEnabled = true;
            PrzyciskF2.IsEnabled = true;
            GridDrzewa.Visibility = Visibility.Hidden;
            GridNawigacjaAllAtOnce.Visibility = Visibility.Hidden;
            GridNawigacjaStepByStep.Visibility = Visibility.Hidden;
            GridWykresy.Visibility = Visibility.Hidden;
            przyciskRestart.Visibility = Visibility.Hidden;
            przyciskZaspisDoPliku.Visibility = Visibility.Hidden;
            liczenieGrafow.CancelAsync();
            CanvasWykresGeneracji.Children.Clear();
            CanvasWykresPareto.Children.Clear();
            CanvasNajgorszeDrzewo.Children.Clear();
            CanvasNajlepszeDrzewo.Children.Clear();
            CanvasSrednieDrzewo.Children.Clear();
            liczenieGrafow.DoWork -= LiczenieGrafow_DoWork;
        }

        private void PowiekszDrzewo_Click(object sender, MouseButtonEventArgs e)
        {
            PowiekszenieDrzewa okno = new PowiekszenieDrzewa();
            okno.Show();
            Drzewo drzewo = (Drzewo)((Canvas)sender).Tag;
            okno.Title = drzewo.KodDrzewa();
            RysujDrzewo(drzewo, okno.canvas);
            RysujWynikiFunkcji(okno.canvas, drzewo, zycie.wagi);
        }

        private void PowiekszPunktPareto_Click(object sender, MouseButtonEventArgs e)
        {
            PowiekszenieDrzewa okno = new PowiekszenieDrzewa();
            okno.Show();
            Drzewo drzewo = (Drzewo)((List<object>)((Ellipse)sender).Tag)[1];
            okno.Title = drzewo.KodDrzewa();
            RysujDrzewo(drzewo, okno.canvas);
            RysujWynikiFunkcji(okno.canvas, drzewo, zycie.wagi);
        }

        private void LiczenieGrafow_DoWork(object sender, DoWorkEventArgs e)
        {
            WielePokolen();
            if (liczenieGrafow.CancellationPending)
            {
                liczenieGrafow.DoWork -= LiczenieGrafow_DoWork;
                e.Cancel = true;
                return;
            }
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            if (liczenieGrafow.IsBusy)
            {
                liczenieGrafow.CancelAsync();
            }
        }
    }
}
