﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasa modelująca algorytm wyznaczania frontu Pareto w teorii grafów.
    /// </summary>
    public class Pareto
    {
        /// <summary>
        /// Funkcja wyznaczająca osobniki tworzące front Pareto.
        /// </summary>
        /// <param name="populacja">Zbiór osobników do sprawdzenia.</param>
        /// <param name="wagi">Tablica wag krawedzi w grafie.</param>
        /// <returns>Lista osobników tworzących front Pareto.</returns>
        public List<Drzewo> WyznaczFrontPareto(List<Drzewo> populacja, Waga[][] wagi)
        {
            List<Drzewo> drzewa = new List<Drzewo>(populacja);
            for (int i = 0; i < drzewa.Count; i++)
            {
                for (int j = 0; j < drzewa.Count; j++)
                {
                    if (CzyDominuje(drzewa[i], drzewa[j], wagi))
                    {
                        drzewa.Remove(drzewa[j]);
                        if (drzewa.Count==i)
                        {
                            break;
                        }
                        if (j<i)
                        {
                            i--;
                        }
                        j--;
                    }
                } //wciaz wypisuje elementy ewidentnie gorsze z obu funkcji
            }
            return drzewa;
        }

        /// <summary>
        /// Funkcja wyznaczająca czy osobnik pierwszy dominuje osobnika drugiego.
        /// </summary>
        /// <param name="drzewo1">Drzewo pierwsze.</param>
        /// <param name="drzewo2">Drzewo drugie</param>
        /// <param name="wagi">Tablica wag krawędzi w grafie.</param>
        /// <returns></returns>
        private bool CzyDominuje(Drzewo drzewo1, Drzewo drzewo2, Waga[][] wagi)
        {
            int a = drzewo1.F1(wagi), b = drzewo1.F2(wagi), c = drzewo2.F1(wagi), d = drzewo2.F2(wagi);
            bool warunek1 = drzewo1.F1(wagi) < drzewo2.F1(wagi) && drzewo1.F2(wagi) <= drzewo2.F2(wagi);
            bool warunek2 = drzewo1.F1(wagi) <= drzewo2.F1(wagi) && drzewo1.F2(wagi) < drzewo2.F2(wagi);
            return warunek1 || warunek2;
        }
    }
}
