﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasa modelująca Wierzchołek w teorii grafów.
    /// </summary>
    public class Wierzcholek : ICloneable
    {
        /// <summary>
        /// Tekstowa etykieta wierzchołka.
        /// </summary>
        public string nazwa;

        /// <summary>
        /// Numer wierzchołka.
        /// </summary>
        public int numer;

        /// <summary>
        /// Lista sąsiadów wierzchołka
        /// </summary>
        public List<int> Sasiedzi
        {
            get { return sasiedzi; }
            private set { }
        }

        /// <summary>
        /// Prywatna lista sąsiadów wierzchołka
        /// </summary>
        private List<int> sasiedzi;

        /// <summary>
        /// Maksymalna liczba sąsiadów, wczytana z interfejsu.
        /// </summary>
        private int maxSasiadow;

        /// <summary>
        /// Metoda dodająca wierzchołek do listy sąsiadów.
        /// </summary>
        /// <param name="sasiad">Wierzchołek, który należy dodać.</param>
        public void DodajSasiada(Wierzcholek sasiad)
        {
            if (sasiedzi.Count<maxSasiadow)
            {
                sasiedzi.Add(sasiad.numer);
            }
            else
            {
                throw new InvalidOperationException("Wierzcholek posiada już " + maxSasiadow + " sąsiadów. Nie można dodać więcej");
            }
        }

        /// <summary>
        /// Metoda usuwająca wierzchołek z listy sąsiadów.
        /// </summary>
        /// <param name="sasiad">Wierzchołek, który należy usunąć.</param>
        public void UsunSasiada(Wierzcholek sasiad)
        {
            sasiedzi.Remove(sasiad.numer);
        }

        public object Clone()
        {
            Wierzcholek kopia = new Wierzcholek(nazwa, numer, maxSasiadow);
            foreach (int sasiad in sasiedzi)
            {
                kopia.sasiedzi.Add(sasiad);
            }
            return kopia;
        }

        /// <summary>
        /// Konstruktor uzupełniający nazwę i inicjalizujący liste sąsiadów.
        /// </summary>
        /// <param name="nazwa">Nazwa wierzchołka.</param>
        /// <param name="numer">Indeks wierzchołkas.</param>
        /// <param name="maxSasiadow">Maksymalna liczba sąsiadów.</param>
        public Wierzcholek(string nazwa, int numer, int maxSasiadow)
        {
            this.nazwa = nazwa;
            this.numer = numer;
            this.maxSasiadow = maxSasiadow;
            sasiedzi = new List<int>(this.maxSasiadow);
        }

        /// <summary>
        /// Domyślny konstruktor.
        /// </summary>
        private Wierzcholek()
        {
            sasiedzi = new List<int>();
        }
    }
}
