﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasa modelująca Drzewo Napinające w teorii grafów.
    /// </summary>
    public class Drzewo : ICloneable
    {
        /// <summary>
        /// Lista opisująca budowę drzewa.`
        /// </summary>
        private List<Krawedz> _krawedzie;

        /// <summary>
        /// Lista krawędzi tworzących drzewo.
        /// </summary>
        public List<Krawedz> krawedzie {
            get {
                return _krawedzie;
            }
            private set { } }

        /// <summary>
        /// Algorytm mutacji drzewa.
        /// </summary>
        private IMutacja algorytmMutacji;

        /// <summary>
        /// Ziarno wymagane przy generowaniu drzewa dla klasy Random.
        /// </summary>
        private static int ziarno = 0;

        private int maxSasiadow;

        /// <summary>
        /// Funkcja mutująca to drzewo.
        /// </summary>
        public void Mutuj()
        {
            algorytmMutacji.Mutuj(_krawedzie, maxSasiadow);
        }
        
        /// <summary>
        /// Funkcja obliczająca sumę wag F1 z wszystkich krawędzi drzewa.
        /// </summary>
        /// <returns>Suma wag F1 w drzewie.</returns>
        public int F1(Waga[][] wagi)
        {
            int suma = 0;
            foreach (Krawedz krawedz in _krawedzie)
            {
                suma += krawedz.F1(wagi);
            }
            return suma;
        }

        /// <summary>
        /// Funkcja obliczająca sumę wag F2 z wszystkich krawędzi drzewa.
        /// </summary>
        /// <returns>Suma wag F2 w drzewie.</returns>
        public int F2(Waga[][] wagi)
        {
            int suma = 0;
            foreach (Krawedz krawedz in _krawedzie)
            {
                suma += krawedz.F2(wagi);
            }
            return suma;
        }

        /// <summary>
        /// Funkcja zwracająca ocenę drzewa - sumę wag F1 i F2.
        /// </summary>
        /// <param name="wagi">Tablica wag krawędzi w grafie.</param>
        /// <returns></returns>
        public int Ocena(Waga[][] wagi)
        {
            return F1(wagi) + F2(wagi);
        }

        /// <summary>
        /// Funkcja zwracająca tekstową reprezentację drzewa (grafu). Wyznaczany jest kod Prufera.
        /// </summary>
        /// <returns>Tekstowy kod drzewa (grafu).</returns>
        public string KodDrzewa()
        {
            string kod = "";
            List<Wierzcholek> wierzcholki = new List<Wierzcholek>();
            foreach (Krawedz krawedz in krawedzie)
            {
                if (!wierzcholki.Any(x => x.numer == krawedz.wierzcholek1.numer))
                {
                    wierzcholki.Add((Wierzcholek)krawedz.wierzcholek1.Clone());
                }
                if (!wierzcholki.Any(x => x.numer == krawedz.wierzcholek2.numer))
                {
                    wierzcholki.Add((Wierzcholek)krawedz.wierzcholek2.Clone());
                }
            }
            wierzcholki = wierzcholki.OrderBy(x => x.numer).ToList();
            do
            {
                Wierzcholek lisc = null;
                for (int i = 0; i < wierzcholki.Count; i++)
                {
                    if (wierzcholki[i].Sasiedzi.Count==1)
                    {
                        lisc = wierzcholki[i];
                        break;
                    }
                }
                Wierzcholek sasiad = wierzcholki.Find(x => x.numer == lisc.Sasiedzi[0]);
                kod += sasiad.nazwa + "-";
                sasiad.UsunSasiada(lisc);
                lisc.UsunSasiada(sasiad);
                wierzcholki.Remove(lisc);
            } while (wierzcholki.Count>=2);
            
            return kod.TrimEnd(new char[] { '-' });
        }

        public object Clone()
        {
            Drzewo kopia = new Drzewo(algorytmMutacji);
            foreach (Krawedz krawedz in _krawedzie)
            {
                kopia.krawedzie.Add(new Krawedz((Wierzcholek)krawedz.wierzcholek1.Clone(), (Wierzcholek)krawedz.wierzcholek2.Clone()));
            }
            return kopia;
        }


        /// <summary>
        /// Konstruktor tworzący losowe drzewo napinające na podanej ilości wierzchołków.
        /// </summary>
        /// <param name="iloscWierzcholkow">Ilość wierzchołków grafu.</param>
        /// <param name="maxSasiadow">Ograniczenie maksymalnej ilości sąsiadów wierzchołka.</param>
        /// <param name="algorytm">Algorytm mutacji drzewa.</param>
        public Drzewo(int iloscWierzcholkow, int maxSasiadow, IMutacja algorytm) : this(algorytm)
        {
            Random rand = new Random(ziarno++ * 1000);
            this.maxSasiadow = maxSasiadow;
            List<Wierzcholek> niepolaczone = new List<Wierzcholek>(iloscWierzcholkow);
            for (int i = 0; i < iloscWierzcholkow; i++)
            {
                Wierzcholek nowy = new Wierzcholek(i.ToString(), i, maxSasiadow);
                niepolaczone.Add(nowy);
            }

            int numerWierzcholkaPoczatkowego = rand.Next(niepolaczone.Count - 1);
            List<Wierzcholek> polaczone = new List<Wierzcholek>();
            polaczone.Add(niepolaczone[numerWierzcholkaPoczatkowego]);
            niepolaczone.RemoveAt(numerWierzcholkaPoczatkowego);

            while(niepolaczone.Count>0)
            {
                int numerWierzcholkaPolaczonego = rand.Next(polaczone.Count - 1);
                int numerWierzcholkaNiepolaczonego = rand.Next(niepolaczone.Count - 1);

                try
                {
                    polaczone[numerWierzcholkaPolaczonego].DodajSasiada(niepolaczone[numerWierzcholkaNiepolaczonego]);
                }
                catch (InvalidOperationException)
                {
                    continue;
                }
                try
                {
                    niepolaczone[numerWierzcholkaNiepolaczonego].DodajSasiada(polaczone[numerWierzcholkaPolaczonego]);
                }
                catch (InvalidOperationException)
                {
                    polaczone[numerWierzcholkaPolaczonego].UsunSasiada(niepolaczone[numerWierzcholkaNiepolaczonego]);
                    continue;
                }
                
                _krawedzie.Add(new Krawedz(polaczone[numerWierzcholkaPolaczonego], niepolaczone[numerWierzcholkaNiepolaczonego]));
                polaczone.Add(niepolaczone[numerWierzcholkaNiepolaczonego]);
                niepolaczone.RemoveAt(numerWierzcholkaNiepolaczonego);
            }
        }

        /// <summary>
        /// Konstruktor uzupełniający algorytmy
        /// </summary>
        /// <param name="algorytm">Algorytm mutacji drzewa.</param>
        private Drzewo(IMutacja algorytm) : this()
        {
            this.algorytmMutacji = algorytm;
        }

        /// <summary>
        /// Domyślny konstruktor. Inicjalizuje pola.
        /// </summary>
        private Drzewo()
        {
            _krawedzie = new List<Krawedz>();
        }
    }

    /// <summary>
    /// Klasa pozwalająca sortować drzewa według ich funkcji oceny.
    /// </summary>
    public class KomparatorDrzew : Comparer<Drzewo>
    {
        Waga[][] wagi;
        public override int Compare(Drzewo x, Drzewo y)
        {
            return x.Ocena(wagi).CompareTo(y.Ocena(wagi));
        }

        public KomparatorDrzew(Waga[][] wagi)
        {
            this.wagi = wagi;
        }
    }
}
