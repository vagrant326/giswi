﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    public class Krawedz
    {
        /// <summary>
        /// Wierchołek początkowy krawędzi.
        /// </summary>
        public Wierzcholek wierzcholek1;

        /// <summary>
        /// Wierzchołek końcowy krawędzi.
        /// </summary>
        public Wierzcholek wierzcholek2;

        /// <summary>
        /// Funkcja zwracająca wartość wagi funkcji F1 dla tej krawędzi.
        /// </summary>
        /// <returns>Wartość wagi funkcji F1 dla tej krawędzi.</returns>
        public int F1(Waga[][] wagi)
        {
            return wagi[wierzcholek1.numer][wierzcholek2.numer].waga1;
        }

        /// <summary>
        /// Funkcja zwracająca wartość wagi funkcji F2 dla tej krawędzi.
        /// </summary>
        /// <returns>Wartość wagi funkcji F2 dla tej krawędzi.</returns>
        public int F2(Waga[][] wagi)
        {
            return wagi[wierzcholek1.numer][wierzcholek2.numer].waga2;
        }

        /// <summary>
        /// Konstruktor tworzący krawędź na podstawie 2 wierzchołków ktore łączy.
        /// </summary>
        /// <param name="wierzcholek1">Wierzchołek początkowy krawędzi.</param>
        /// <param name="wierzcholek2">Wierzchołek końcowy krawędzi.</param>
        public Krawedz(Wierzcholek wierzcholek1, Wierzcholek wierzcholek2)
        {
            this.wierzcholek1 = wierzcholek1;
            this.wierzcholek2 = wierzcholek2;
        }
    }
}
