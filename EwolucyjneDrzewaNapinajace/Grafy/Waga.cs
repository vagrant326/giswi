﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EwolucyjneDrzewaNapinajace
{
    /// <summary>
    /// Klasa modelująca wartości opisujące krawędź w grafie.
    /// </summary>
    public class Waga
    {
        /// <summary>
        /// Waga funkcji F1.
        /// </summary>
        public int waga1;

        /// <summary>
        /// Waga funkcji F2.
        /// </summary>
        public int waga2;


        /// <summary>
        /// Konstruktor inicjalizujący wszystkie wartości w klasie.
        /// </summary>
        /// <param name="waga1">Waga funkcji F1.</param>
        /// <param name="waga2">Waga funkcji F2.</param>
        public Waga(int waga1, int waga2)
        {
            this.waga1 = waga1;
            this.waga2 = waga2;
        }
    }
}
